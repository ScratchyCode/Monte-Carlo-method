// Coded by ScratchyCode
// Compile in gcc with option -lm
#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>

#define PI 3.1415926535

double error(double average);

int main(){
    long long int i, dim; 
    double pi, x, y, count=0;
    srand(time(NULL));
    
    // run as root to get this setting
    // WARNING: the computer may be slowed down
    nice(-20);
    
    printf("\nEnter the max number of random variables: ");
    scanf("%lld",&dim);
    
    // look for the random positions of the points P (x, y)
    for(i=0; i<=dim; i++){
        x = pow(((double)(rand()) / ((double)RAND_MAX + 1.0)),2);
        y = pow(((double)(rand()) / ((double)RAND_MAX + 1.0)),2);
        if(x + y <= 1){
            count++;
        }
    }
    
    pi = 4 * (count/dim);
    printf("Pi approximation:\t%0.10lf\n", pi);
    printf("Related error:\t\t%0.5lf%%\n",error(pi));

	return 0;
}

double error(double average){
    double E, Ea, Er, approx;
    
    E = average - PI;
    Ea = fabs(E);
    Er = Ea/average;
    approx = Er*100;
    
    return approx;
}
