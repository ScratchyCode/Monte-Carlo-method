// Coded by ScratchyCode
// Compile in gcc with option -lm
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>

#define EXP 2.7182818284

int stochastic();
double error(double average);

int main(){
    long long int i, n, sum=0;
    double num, denom, average;
    int *vect;
    char file[] = "Data.txt";
    srand(time(NULL));
    
    printf("\nEnter the number of partial sums to be counted: ");
    scanf("%lld", &n);
    
    // dynamically allocation of memory
    vect = malloc(n * sizeof(int));
    if(vect == NULL){
        perror("\nError");
        printf("\n");
        exit(1);
    }
    
    // calculation of random values
    for(i=0; i<=n; i++){
        vect[i] = stochastic();
    }

  	// average of found values
    for(i=0; i<=n; i++){
        sum = sum + vect[i];
    }
    
    num = (double)sum;
    denom = (double)n;
    average = num/denom;

	printf("\nApproximation of e:\t%0.10lf\n",average);
    printf("Related error:\t\t%0.5lf%%\n",error(average));
    
    // writing results on a file to get statistical data
    FILE *pf;
    pf = fopen(file, "a+");
    if(pf == NULL){
        perror("\nError");
        printf("\n");
        exit(1);
    }

    fprintf(pf,"N = %lld", n);
    fprintf(pf,"\ne = %lf", average);
    fprintf(pf,"\nerror = %0.4lf%%\n\n", error(average));
    fflush(pf);
    fclose(pf);
    free(vect);
    vect = NULL;
    printf("\nResult written on '%s' file.\n",file);
    
    return 0;
}

int stochastic(){
    long long int count=0;
    double X, S=0;
    
    while(S<1.0){
        X = (double)(rand()) / ((double)RAND_MAX + 1.0);
        S = S + X;
        count++;
    }

    return count;
}

double error(double average){
    double E, Ea, Er, approx;
    
    E = average - EXP;
    Ea = fabs(E);
    Er = Ea/average;
    approx = Er*100;
    
    return approx;
}
